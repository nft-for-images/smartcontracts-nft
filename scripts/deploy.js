const hre = require("hardhat");

async function main() {

  const Nftforimages = await hre.ethers.getContractFactory("Nftforimages");
  const nftforimages = await Nftforimages.deploy();

  await nftforimages.deployed();

  console.log(
    `deployed contract address ${nftforimages.address}`
  );
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});