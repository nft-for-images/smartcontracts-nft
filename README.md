# SmartContracts-NFT
## NFT Smart Contract (ERC721) Development
--------------
## Deployment
```html
npm install @openzeppelin/contracts
```

```html 
npx hardhat node
  ```
  ```html
  npx hardhat compile
  ```
  ```html
  npx hardhat run --network localhost scripts/deploy.js
```
